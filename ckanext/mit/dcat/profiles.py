from builtins import str
from past.builtins import basestring
from builtins import object
import datetime
import json

import six
from six.moves.urllib.parse import quote

from dateutil.parser import parse as parse_date

from ckantoolkit import config

import rdflib
from rdflib import URIRef, BNode, Literal
from rdflib.namespace import Namespace, RDF, XSD, SKOS, RDFS

from geomet import wkt, InvalidGeoJSONException

from ckan.model.license import LicenseRegister
from ckan.plugins import toolkit
from ckan.lib.munge import munge_tag
from ckan.lib.helpers import url_for

from ckanext.dcat.utils import resource_uri, dataset_uri, catalog_uri, publisher_uri_organization_fallback, DCAT_EXPOSE_SUBCATALOGS, DCAT_CLEAN_TAGS
from ckanext.dcat.profiles import RDFProfile, URIRefOrLiteral, CleanedURIRef

DCT = Namespace("http://purl.org/dc/terms/")
DCAT = Namespace("http://www.w3.org/ns/dcat#")
DCATAPIT = Namespace("http://dati.gov.it/onto/dcatapit#")
ADMS = Namespace("http://www.w3.org/ns/adms#")
VCARD = Namespace("http://www.w3.org/2006/vcard/ns#")
FOAF = Namespace("http://xmlns.com/foaf/0.1/")
SCHEMA = Namespace('http://schema.org/')
TIME = Namespace('http://www.w3.org/2006/time')
LOCN = Namespace('http://www.w3.org/ns/locn#')
GSP = Namespace('http://www.opengis.net/ont/geosparql#')
OWL = Namespace('http://www.w3.org/2002/07/owl#')
SPDX = Namespace('http://spdx.org/rdf/terms#')

GEOJSON_IMT = 'https://www.iana.org/assignments/media-types/application/vnd.geo+json'

namespaces = {
    'dct': DCT,
    'dcat': DCAT,
    'dcatapit': DCATAPIT,
    'adms': ADMS,
    'vcard': VCARD,
    'foaf': FOAF,
    'schema': SCHEMA,
    'time': TIME,
    'skos': SKOS,
    'locn': LOCN,
    'gsp': GSP,
    'owl': OWL,
    'spdx': SPDX,
}

PREFIX_MAILTO = u'mailto:'

class ItalianDCATAPProfile(RDFProfile):
    '''
    An RDF profile based on the DCAT-AP for data portals in Italy
    More information and specification:
    https://docs.italia.it/italia/daf/linee-guida-cataloghi-dati-dcat-ap-it/it/stabile/dcat-ap_it.html
    '''
    def graph_from_dataset(self, dataset_dict, dataset_ref):

        g = self.g

        for prefix, namespace in namespaces.items():
            g.bind(prefix, namespace)

        dataset_ref = CleanedURIRef(dataset_uri(dataset_dict))

        # g.add((dataset_ref, RDF.type, DCAT.Dataset))
        g.add((dataset_ref, RDF.type, DCATAPIT.Dataset))

        # Basic fields
        items = [
            ('title', DCT.title, None, Literal),
            ('notes', DCT.description, None, Literal),
            # ('url', DCAT.landingPage, None, URIRef),
            ('identifier', DCT.identifier, ['guid', 'id'], Literal),
            ('version', OWL.versionInfo, ['dcat_version'], Literal),
            # ('version_notes', ADMS.versionNotes, None, Literal),
            # ('frequency', DCT.accrualPeriodicity, None, URIRefOrLiteral),
            # ('access_rights', DCT.accessRights, None, Literal),
            # ('dcat_type', DCT.type, None, Literal),
            # ('provenance', DCT.provenance, None, Literal),
        ]
        self._add_triples_from_dict(dataset_dict, dataset_ref, items)

        # Tags
        for tag in dataset_dict.get('tags', []):
            g.add((dataset_ref, DCAT.keyword, Literal(tag['name'])))

        # Dates
        items = [
            ('issued', DCT.issued, ['metadata_created'], Literal),
            ('modified', DCT.modified, ['metadata_modified'], Literal),
        ]
        self._add_date_triples_from_dict(dataset_dict, dataset_ref, items)

        #  Lists
        items = [
            # ('language', DCT.language, None, URIRefOrLiteral),
            # ('theme', DCAT.theme, None, URIRef),
            ('conforms_to', DCT.conformsTo, None, Literal),
            ('alternate_identifier', ADMS.identifier, None, Literal),
            ('documentation', FOAF.page, None, URIRefOrLiteral),
            ('related_resource', DCT.relation, None, URIRefOrLiteral),
            ('has_version', DCT.hasVersion, None, URIRefOrLiteral),
            ('is_version_of', DCT.isVersionOf, None, URIRefOrLiteral),
            ('source', DCT.source, None, Literal),
            ('sample', ADMS.sample, None, Literal),
        ]
        self._add_list_triples_from_dict(dataset_dict, dataset_ref, items)

        # Contact details
        if any([
            self._get_dataset_value(dataset_dict, 'contact_uri'),
            self._get_dataset_value(dataset_dict, 'contact_name'),
            self._get_dataset_value(dataset_dict, 'contact_email'),
            self._get_dataset_value(dataset_dict, 'maintainer'),
            self._get_dataset_value(dataset_dict, 'maintainer_email'),
            self._get_dataset_value(dataset_dict, 'author'),
            self._get_dataset_value(dataset_dict, 'author_email'),
        ]):

            contact_uri = self._get_dataset_value(dataset_dict, 'contact_uri')
            if contact_uri:
                contact_details = CleanedURIRef(contact_uri)
            else:
                contact_details = BNode()

            g.add((contact_details, RDF.type, VCARD.Organization))
            g.add((dataset_ref, DCAT.contactPoint, contact_details))

            self._add_triple_from_dict(
                dataset_dict, contact_details,
                VCARD.fn, 'contact_name', ['maintainer', 'author']
            )
            # Add mail address as URIRef, and ensure it has a mailto: prefix
            self._add_triple_from_dict(
                dataset_dict, contact_details,
                VCARD.hasEmail, 'contact_email', ['maintainer_email',
                                                  'author_email'],
                _type=URIRef, value_modifier=self._add_mailto
            )

        # Publisher
        if any([
            # self._get_dataset_value(dataset_dict, 'publisher_uri'),
            # self._get_dataset_value(dataset_dict, 'publisher_name'),
            dataset_dict.get('organization'),
        ]):
            org = dataset_dict.get('organization')
            org_dict = toolkit.get_action('organization_show')({}, {'id': org.get('id'), 'include_datasets': False})
            # print('org', org_dict)
            # publisher_uri = self._get_dataset_value(dataset_dict, 'publisher_uri')
            publisher_uri = publisher_uri_organization_fallback(dataset_dict)
            # publisher_uri_fallback = publisher_uri_organization_fallback(dataset_dict)
            # publisher_name = self._get_dataset_value(dataset_dict, 'publisher_name')
            if publisher_uri:
                publisher_details = CleanedURIRef(publisher_uri)
            # elif not publisher_name and publisher_uri_fallback:
            #     # neither URI nor name are available, use organization as fallback
            #     publisher_details = CleanedURIRef(publisher_uri_fallback)
            else:
                # No publisher_uri
                publisher_details = BNode()

            # g.add((publisher_details, RDF.type, FOAF.Organization))
            g.add((publisher_details, RDF.type, DCATAPIT.Agent))
            g.add((dataset_ref, DCT.publisher, publisher_details))

            # # In case no name and URI are available, again fall back to organization.
            # # If no name but an URI is available, the name literal remains empty to
            # # avoid mixing organization and dataset values.
            # if not publisher_name and not publisher_uri and dataset_dict.get('organization'):
            #     publisher_name = dataset_dict['organization']['title']

            # g.add((publisher_details, FOAF.name, Literal(publisher_name)))
            g.add((publisher_details, FOAF.name, Literal(org.get('title'))))
            g.add((publisher_details, DCT.identifier, Literal(org.get('name'))))

            # TODO: It would make sense to fallback these to organization
            # fields but they are not in the default schema and the
            # `organization` object in the dataset_dict does not include
            # custom fields
            # items = [
            #     ('publisher_email', FOAF.mbox, None, Literal),
            #     ('publisher_email', FOAF.mbox, None, Literal),
            #     ('publisher_url', FOAF.homepage, None, URIRef),
            #     ('publisher_type', DCT.type, None, URIRefOrLiteral),
            # ]
            # self._add_triples_from_dict(dataset_dict, publisher_details, items)

            # Rights holder
            g.add((dataset_ref, DCT.rightsHolder, publisher_details))
            # Creator
            g.add((dataset_ref, DCT.creator, publisher_details))

        # Temporal
        start = self._get_dataset_value(dataset_dict, 'temporal_start')
        end = self._get_dataset_value(dataset_dict, 'temporal_end')
        if start or end:
            temporal_extent = BNode()

            g.add((temporal_extent, RDF.type, DCT.PeriodOfTime))
            if start:
                self._add_date_triple(temporal_extent, SCHEMA.startDate, start)
            if end:
                self._add_date_triple(temporal_extent, SCHEMA.endDate, end)
            g.add((dataset_ref, DCT.temporal, temporal_extent))

        # Spatial
        spatial_uri = self._get_dataset_value(dataset_dict, 'spatial_uri')
        spatial_text = self._get_dataset_value(dataset_dict, 'spatial_text')
        spatial_geom = self._get_dataset_value(dataset_dict, 'spatial')

        if spatial_uri or spatial_text or spatial_geom:
            if spatial_uri:
                spatial_ref = CleanedURIRef(spatial_uri)
            else:
                spatial_ref = BNode()

            g.add((spatial_ref, RDF.type, DCT.Location))
            g.add((dataset_ref, DCT.spatial, spatial_ref))

            if spatial_text:
                g.add((spatial_ref, SKOS.prefLabel, Literal(spatial_text)))

            if spatial_geom:
                # GeoJSON
                g.add((spatial_ref,
                       LOCN.geometry,
                       Literal(spatial_geom, datatype=GEOJSON_IMT)))
                # WKT, because GeoDCAT-AP says so
                try:
                    g.add((spatial_ref,
                           LOCN.geometry,
                           Literal(wkt.dumps(json.loads(spatial_geom),
                                             decimals=4),
                                   datatype=GSP.wktLiteral)))
                except (TypeError, ValueError, InvalidGeoJSONException):
                    pass

        # Landing page
        landing_page = URIRef(dataset_uri(dataset_dict))
        g.add((landing_page, RDF.type, FOAF.Document))
        g.add((dataset_ref, DCAT.landingPage, landing_page))

        # Language
        g.add((dataset_ref, DCT.language, URIRef(config.get('ckanext.mit.authority_language')+config.get('ckanext.mit.authority_language_default'))))

        # Accrual periodicity
        frequency = dataset_dict.get('frequency', config.get('ckanext.mit.authority_frequency_default'))
        g.add((dataset_ref, DCT.accrualPeriodicity, URIRef(config.get('ckanext.mit.authority_frequency')+frequency)))

        # Theme
        theme = dataset_dict.get('theme', config.get('ckanext.mit.authority_theme_default'))
        g.add((dataset_ref, DCAT.theme, URIRef(config.get('ckanext.mit.authority_theme')+theme)))
        
        # License
        license_url = dataset_dict.get('license_url')
        if license_url:
            license_details = URIRef(license_url)
            g.add((license_details, RDF.type, DCATAPIT.LicenseDocument))
            g.add((license_details, RDF.type, DCT.LicenseDocument))
            g.add((license_details, DCT.identifier, Literal(dataset_dict.get('license_id'))))
            g.add((license_details, FOAF.name, Literal(dataset_dict.get('license_title'))))
            # # https://joinup.ec.europa.eu/svn/adms/ADMS_v1.00/ADMS_SKOS_v1.00.html
            # license_types = {
            #         'CC-BY-4.0':'http://purl.org/adms/licencetype/Attribution'
            # }
            # if license_types.get(dataset_dict.get('license_id')) is not None:
            #     g.add((license_details, DCT.type, URIRef(license_types.get(dataset_dict.get('license_id')))))

        # Resources
        for resource_dict in dataset_dict.get('resources', []):

            distribution = CleanedURIRef(resource_uri(resource_dict))
            # distribution = URIRef(resource_uri(resource_dict).replace('/catalog/', '/dcat/'))

            g.add((dataset_ref, DCAT.distribution, distribution))

            # g.add((distribution, RDF.type, DCAT.Distribution))
            g.add((distribution, RDF.type, DCATAPIT.Distribution))

            #  Simple values
            items = [
                ('name', DCT.title, None, Literal),
                ('description', DCT.description, None, Literal),
                # ('status', ADMS.status, None, URIRefOrLiteral),
                # ('rights', DCT.rights, None, URIRefOrLiteral),
                # ('license', DCT.license, None, URIRefOrLiteral),
                # ('access_url', DCAT.accessURL, None, URIRef),
                # ('download_url', DCAT.downloadURL, None, URIRef),
            ]
            self._add_triples_from_dict(resource_dict, distribution, items)

            # License
            if license_url:
                g.add((distribution, DCT.license, license_details))

            # #  Lists
            # items = [
            #     ('documentation', FOAF.page, None, URIRefOrLiteral),
            #     ('language', DCT.language, None, URIRefOrLiteral),
            #     ('conforms_to', DCT.conformsTo, None, Literal),
            # ]
            # self._add_list_triples_from_dict(resource_dict, distribution, items)

            # Format
            mimetype = resource_dict.get('mimetype')
            fmt = resource_dict.get('format')

            # # IANA media types (either URI or Literal) should be mapped as mediaType.
            # # In case format is available and mimetype is not set or identical to format,
            # # check which type is appropriate.
            # if fmt and (not mimetype or mimetype == fmt):
            #     if ('iana.org/assignments/media-types' in fmt
            #             or not fmt.startswith('http') and '/' in fmt):
            #         # output format value as dcat:mediaType instead of dct:format
            #         mimetype = fmt
            #         fmt = None
            #     else:
            #         # Use dct:format
            #         mimetype = None

            if mimetype:
                g.add((distribution, DCAT.mediaType, URIRefOrLiteral(mimetype)))

            if fmt:
                g.add((distribution, DCT['format'], URIRef('http://publications.europa.eu/resource/authority/file-type/'+fmt)))
                # g.add((distribution, DCT['format'], URIRefOrLiteral(fmt)))


            # URL fallback and old behavior
            url = resource_dict.get('url')
            download_url = resource_dict.get('download_url')
            access_url = resource_dict.get('access_url')
            # Use url as fallback for access_url if access_url is not set and download_url is not equal
            if url and not access_url:
                if (not download_url) or (download_url and url != download_url):
                  self._add_triple_from_dict(resource_dict, distribution, DCAT.accessURL, 'url', _type=URIRef)

            # Dates
            items = [
                ('issued', DCT.issued, None, Literal),
                ('modified', DCT.modified, None, Literal),
            ]
            self._add_date_triples_from_dict(resource_dict, distribution, items)

            # Numbers
            if resource_dict.get('size'):
                try:
                    g.add((distribution, DCAT.byteSize,
                           Literal(float(resource_dict['size']),
                                   datatype=XSD.decimal)))
                except (ValueError, TypeError):
                    g.add((distribution, DCAT.byteSize,
                           Literal(resource_dict['size'])))
            # Checksum
            if resource_dict.get('hash'):
                checksum = BNode()
                g.add((checksum, RDF.type, SPDX.Checksum))
                g.add((checksum, SPDX.checksumValue,
                       Literal(resource_dict['hash'],
                               datatype=XSD.hexBinary)))

                if resource_dict.get('hash_algorithm'):
                    g.add((checksum, SPDX.algorithm,
                           URIRefOrLiteral(resource_dict['hash_algorithm'])))

                g.add((distribution, SPDX.checksum, checksum))

    def graph_from_catalog(self, catalog_dict, catalog_ref):

        g = self.g

        for prefix, namespace in namespaces.items():
            g.bind(prefix, namespace)

        catalog_ref = CleanedURIRef(catalog_uri())

        # g.add((catalog_ref, RDF.type, DCAT.Catalog))
        g.add((catalog_ref, RDF.type, DCATAPIT.Catalog))
        # Basic fields
        items = [
            ('title', DCT.title, config.get('ckan.site_title'), Literal),
            ('description', DCT.description, config.get('ckan.site_description'), Literal),
            ('homepage', FOAF.homepage, config.get('ckan.site_url'), URIRef),
            ('language', DCT.language, config.get('ckanext.mit.authority_language')+config.get('ckanext.mit.authority_language_default'), URIRef),
            ('themeTaxonomy', DCT.themeTaxonomy, config.get('ckanext.mit.authority_theme'), URIRef),
        ]
        for item in items:
            key, predicate, fallback, _type = item
            # if catalog_dict:
            #     value = catalog_dict.get(key, fallback)
            # else:
            #     value = fallback
            # if value:
            #     g.add((catalog_ref, predicate, _type(value)))
            value = fallback
            g.add((catalog_ref, predicate, _type(value)))

        publisher_details = URIRef(config.get('ckanext.mit.dct_publisher'))
        g.add((publisher_details, OWL.sameAs, URIRef(config.get('ckanext.mit.dct_publisher.owl_sameas'))))
        g.add((publisher_details, RDF.type, DCATAPIT.Agent))
        g.add((publisher_details, RDF.type, FOAF.Agent))
        g.add((publisher_details, DCT.identifier, Literal(config.get('ckanext.mit.dct_publisher.dct_identifier'))))
        g.add((publisher_details, FOAF.name, Literal(config.get('ckanext.mit.dct_publisher.foaf_name'))))
        g.add((catalog_ref, DCT.publisher, publisher_details))

        # Dates
        modified = self._last_catalog_modification()
        if modified:
            self._add_date_triple(catalog_ref, DCT.modified, modified)